/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const { tryCatchWrapper } = require('../utils/apiUtils');
const { authorize } = require('../middlewares/authMiddleware');

const {
  addUserLoad,
  getUserLoads,
  getUserLoadById,
  getUserActiveLoad,
  statesOfLoad,
  iterateLoadState,
  updateUserLoadById,
} = require('../services/loadServices');

const { Truck } = require('../models/truckModel');
// const { Load } = require('../models/loadModel');

router.post(
    '/',
    authorize('SHIPPER'),
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      const loadPayload = { ...req.body };

      await addUserLoad(loadPayload, _id);

      res.json({ message: 'Load was created succesfully' });
    }),
);

router.get(
    '/',
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;

      const loads = await getUserLoads(_id);
      res.json({ loads: loads });
    }),
);


router.get(
    '/active',
    authorize('DRIVER'),
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;

      const load = await getUserActiveLoad(_id);
      res.json({ load: load });
    }),
);


router.patch(
    '/active/state',
    authorize('DRIVER'),
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      const truck = await Truck.findOne({ assigned_to: _id });

      const load = await iterateLoadState(_id);


      if (load.state === 'Arrived to delivery') {
        load.status = 'SHIPPED';
        load.assigned_to = null;
        truck.status = 'IS';
      }

      await load.save();
      await truck.save();

      res.json({ message: `Load state changed to ${load.state}` });
    }),
);

router.get(
    '/:id',
    tryCatchWrapper(async (req, res) => {
      const loadId = req.params.id;
      const { _id } = req.user;

      const load = await getUserLoadById(loadId, _id);

      res.json({ load: load });
    }),
);


router.put(
    '/:id',
    authorize('SHIPPER'),
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      const loadId = req.params.id;
      const data = req.body;


      const load = await updateUserLoadById(loadId, _id, data);

      res.json({ load });
    }),
);

router.post('/:id/post',
    tryCatchWrapper(async (req, res) => {
      const loadId = req.params.id;
      const { _id } = req.user;

      const load = await getUserLoadById(loadId, _id);
      load.status = 'POSTED';

      await load.save();

      const truck = await Truck.findOne({
        'assigned_to': { $ne: null },
        'status': { $eq: 'IS' },
        'dimensions.width': { $gte: load.dimensions.width },
        'dimensions.length': { $gte: load.dimensions.length },
        'dimensions.height': { $gte: load.dimensions.height },
        'dimensions.capacity': { $gte: load.payload },
      });


      if (!truck) {
        load.status = 'NEW';
        load.logs.push({ message: 'Truck was not found' });
        await load.save();
        throw new Error('Truck not found');
      } else {
        truck.status = 'OL';
        load.status = 'ASSIGNED';
        load.state = statesOfLoad[0];
        load.assigned_to = truck.assigned_to;
        load.logs.push({
          message: `Load assigned to driver with id ${truck.assigned_to}`,
          time: Date.now(),
        });

        await truck.save();
        await load.save();

        res.json({
          'message': 'Load posted successfully',
          'driver_found': true,
        });
      }
    }),
);


router.delete(
    '/:id',
    authorize('SHIPPER'),
    tryCatchWrapper(async (req, res) => {
      const loadId = req.params.id;
      const { _id } = req.user;

      const load = await getUserLoadById(loadId, _id);
      await load.remove();

      res.json({ 'message': 'Load deleted successfully' });
    }),
);

module.exports = {
  loadRouter: router,
};
