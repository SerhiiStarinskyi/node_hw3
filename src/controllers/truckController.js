/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const { tryCatchWrapper } = require('../utils/apiUtils');
const {
  addUserTruck,
  getUserTrucks,
  getUserTruckById,
  updateUserTruckById,
  assignUserTruckById,
  // isAssignd,
} = require('../services/truckServices');

router.post(
    '/',
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      const { type } = req.body;

      await addUserTruck(_id, type);

      res.json({ message: 'Truck was created succesfully' });
    }),
);

router.get(
    '/',
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;

      const trucks = await getUserTrucks(_id);

      res.json({ trucks: trucks });
    }),
);

router.get(
    '/:id',
    tryCatchWrapper(async (req, res) => {
      const truckId = req.params.id;
      const { _id } = req.user;

      const truck = await getUserTruckById(truckId, _id);

      res.json({ truck: truck });
    }),
);

router.put(
    '/:id',
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      const truckId = req.params.id;
      const data = req.body;

      // if (isAssignd(truckId, _id)) {
      //   throw new Error("You can't make any changes");
      // }

      await updateUserTruckById(truckId, _id, data);

      res.json({ message: 'Truck details changed successfully' });
    }),
);

router.post(
    '/:id/assign',
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      const truckId = req.params.id;

      await assignUserTruckById(truckId, _id);

      res.json({ message: 'Truck assigned successfully' });
    }),
);

router.delete(
    '/:id',
    tryCatchWrapper(async (req, res) => {
      const truckId = req.params.id;
      const { _id } = req.user;

      const truck = await getUserTruckById(truckId, _id);

      if (truck.assigned_to) {
        throw new Error('You can not delete truck that is assigned to you');
      }

      await truck.remove();
      // await deleteUserTruckById(truckId, _id);

      res.json({ message: 'Truck deleted successfully' });
    }),
);

module.exports = {
  truckRouter: router,
};
