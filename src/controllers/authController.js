/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const { login, createProfile } = require('../services/authUser');
// const {
//   registrationValidator,
// } = require('../middlewares/validationMiddleware');
const { tryCatchWrapper } = require('../utils/apiUtils');

router.post(
    '/auth/register',
    // registrationValidator,
    tryCatchWrapper(async (req, res) => {
      const { email, password, role } = req.body;

      await createProfile({ email, password, role });
      res.json({ message: `Account was created succesfully!` });
    }),
);

router.post(
    '/auth/login',
    tryCatchWrapper(async (req, res) => {
      const { email, password } = req.body;

      const token = await login({ email, password });

      res.json({ message: `Logged in succesfully!`, jwt_token: token });
    }),
);

module.exports = {
  authRouter: router,
};
