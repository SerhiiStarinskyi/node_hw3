/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const { tryCatchWrapper } = require('../utils/apiUtils');
const { getProfileInfo, deleteProfile } = require('../services/userServices');

router.get(
    '/users/me',
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      const user = await getProfileInfo(_id);
      res.json({ user: user });
    }),
);

router.delete(
    '/users/me',
    tryCatchWrapper(async (req, res) => {
      const { _id } = req.user;
      await deleteProfile(_id);
      res.json({ message: 'Profile deleted successfully' });
    }),
);

module.exports = {
  userRouter: router,
};
