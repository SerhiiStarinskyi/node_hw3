const jwt = require('jsonwebtoken');

require('dotenv').config({ path: '../src/config/.env' });
const JWT_SECRET = process.env.JWT_SECRET;

const authMiddleware = (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res
        .status(401)
        .json({ message: 'Please, provide "authorization" header' });
  }

  // Bearer - тип токена, token
  const [, token] = authorization.split(' ');

  if (!token) {
    return res
        .status(401)
        .json({ message: 'Please, include token to request' });
  }

  try {
    // tokenPayLoad - user data
    const tokenPayLoad = jwt.verify(token, JWT_SECRET);

    req.user = {
      created_by: tokenPayLoad._id,
      _id: tokenPayLoad._id,
      email: tokenPayLoad.email,
      role: tokenPayLoad.role,
    };


    next();
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};

// Grant access to specific roles

const authorize = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(new Error('User has no role'));
    }

    next();
  };
};

module.exports = {
  authMiddleware,
  authorize,
};
