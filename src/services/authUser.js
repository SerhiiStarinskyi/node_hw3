const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

require('dotenv').config({ path: '../src/config/.env' });
const JWT_SECRET = process.env.JWT_SECRET;

const { User } = require('../models/userModel');

// @desc      Register user
// route      POST 'api/auth/register'
// @access    Public
const createProfile = async ({ email, password, role }) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();
};

// @desc      Login user
// route      POST 'api/auth/login'
// @access    Private
const login = async ({ email, password }) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password');
  }

  const token = jwt.sign(
      {
        _id: user._id,
        email: user.email,
        role: user.role,
      },
      JWT_SECRET,
  );

  return token;
};

module.exports = {
  login,
  createProfile,
};
