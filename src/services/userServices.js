const { User } = require('../models/userModel');

// @desc        Get user profile by ID
// @route       GET api/users/me
// @access      Private
const getProfileInfo = async (_id) => {
  // const user = await User.findOne({ _id });
  const user = await User.findById({ _id }).select('-__v -password');
  return user;
};

// @desc        Get user profile by ID
// @route       DELETE api/users/me
// @access      Private
const deleteProfile = async (_id) => {
  await User.remove({ _id });
};

module.exports = {
  getProfileInfo,
  deleteProfile,
};
