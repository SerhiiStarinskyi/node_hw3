const { Truck } = require('../models/truckModel');

const checkDimensionsOfTruck = (type) => {
  const dimensions = {
    'SPRINTER': { length: 300, width: 250, height: 170, capacity: 1700 },
    'SMALL STRAIGHT': { length: 500, width: 250, height: 170, capacity: 2500 },
    'LARGE STRAIGHT': { length: 700, width: 350, height: 200, capacity: 4000 },
  };

  return dimensions[type];
};

const isAssignd = async (truckId, _id) => {
  const truck = await getUserTruckById(truckId, _id);

  if (truck.assigned_to) {
    return false;
  }
  return true;
};

const addUserTruck = async (userId, typeOfTruck) => {
  const truck = new Truck({
    created_by: userId,
    type: typeOfTruck,
    dimensions: checkDimensionsOfTruck(typeOfTruck),
  });

  await truck.save();
};

const getUserTrucks = async (userId) => {
  const truck = await Truck.find(
      { created_by: userId },
      { dimensions: 0, __v: 0 },
  );
  return truck;
};

const getUserTruckById = async (truckId, userId) => {
  const truck = await Truck.findOne(
      { _id: truckId, created_by: userId },
      { dimensions: 0, __v: 0 },
  );
  return truck;
};

const updateUserTruckById = async (truckId, userId, data) => {
  await Truck.findOneAndUpdate(
      { _id: truckId, created_by: userId },
      { $set: data },
  );
};

const assignUserTruckById = async (truckId, userId) => {
  await Truck.findOneAndUpdate(
      { _id: truckId, created_by: userId },
      { $set: { assigned_to: userId } },
  );
};

module.exports = {
  addUserTruck,
  getUserTrucks,
  getUserTruckById,
  updateUserTruckById,
  assignUserTruckById,
  isAssignd,
};
