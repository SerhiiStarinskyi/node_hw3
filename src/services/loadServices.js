const { Load } = require('../models/loadModel');
// const { Truck } = require('../models/truckModel');
const statesOfLoad = ['En route to Pick Up', 'Arrived to Pick Up',
  'En route to delivery', 'Arrived to delivery'];

const addUserLoad = async (loadPayload, userId) => {
  const load = new Load({
    created_by: userId,
    ...loadPayload,
  });

  await load.save();
};

const getUserLoads = async (userId) => {
  const load = await Load.find({ created_by: userId }, { __v: 0 });
  return load;
};


const getUserActiveLoad = async (userId) => {
  const load = await Load.findOne({ assigned_to: userId }, { __v: 0 });
  return load;
};

const iterateLoadState = async (userId) => {
  const load = await Load.findOne({ assigned_to: userId });

  const stateOfCurrentLoad = load.state;
  // console.log('LOAD STATE: ', load.state);
  for (let i = 0; i < statesOfLoad.length; i++) {
    if (stateOfCurrentLoad === statesOfLoad[i]) {
      load.state = statesOfLoad[i+1];
      console.log(statesOfLoad[i+1]);


      break;
    }
  };

  await load.save();
  return load;
};

const getUserLoadById = async (loadId, userId) => {
  const load = await Load.findOne(
      { _id: loadId, created_by: userId },
      { __v: 0 },
  );
  return load;
};

const updateUserLoadById = async (loadId, userId, data) => {
  return await Load.findOneAndUpdate(
      { _id: loadId, created_by: userId },
      { $set: data },
  );
};

module.exports = {
  addUserLoad,
  getUserLoads,
  getUserLoadById,
  // postUserLoadById,
  getUserActiveLoad,
  statesOfLoad,
  iterateLoadState,
  updateUserLoadById,
};
