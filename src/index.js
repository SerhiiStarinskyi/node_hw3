require('dotenv').config({ path: './src/config/.env' });
const PORT = process.env.PORT;
const DB_PATH = process.env.DB_PATH;

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

const { authRouter } = require('./controllers/authController');
const { userRouter } = require('./controllers/userControllers');
const { truckRouter } = require('./controllers/truckController');
const { loadRouter } = require('./controllers/loadController');
const { authMiddleware, authorize } = require('./middlewares/authMiddleware');

app.use(express.json()); // for parsing application/json
app.use(express.text());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('tiny'));

app.use('/api', authRouter);

app.use(authMiddleware);
app.use('/api', userRouter);
app.use('/api/trucks', authorize('DRIVER'), truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    await mongoose.connect(DB_PATH, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
