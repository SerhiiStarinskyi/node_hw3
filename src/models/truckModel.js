const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  assigned_to: {
    type: String,
    default: null,
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    default: 'IS',
    enum: ['IS', 'OL'],
  },
  dimensions: Object,
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = { Truck };
