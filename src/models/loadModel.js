const mongoose = require('mongoose');
const { statesOfLoad } = require('../services/loadServices');

const Load = mongoose.model('Load', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    default: null,
  },
  status: {
    type: String,
    default: 'NEW',
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    default: null,
    enum: [statesOfLoad, null],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {

    width: { type: Number },
    length: { type: Number },
    height: { type: Number },
  },

  logs: [
    { message: {
      type: String,
    },

    time: {
      type: Date,
      default: Date.now() },
    },
  ],

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {
  Load,
};
