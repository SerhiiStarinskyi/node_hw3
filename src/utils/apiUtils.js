const tryCatchWrapper = (clb) => {
  return (req, res, next) => clb(req, res).catch(next);
};

module.exports = {
  tryCatchWrapper,
};
